import React, { useEffect } from 'react';
// import { Button, Alert } from 'reactstrap';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch} from "react-router-dom";
// import './component/login/style.css';
// import UserProfile from "./component/userProfile";
// import OneClock from "./component/clock/OneClock";
// import TryHook from "./component/clock/TryHook";
// import UseEffect from "./component/clock/UseEffect";
// import ConRendering from "./component/latihan/ConRendering"
// import Form from "./component/latihan/Form";
import Login from "./component/login/Login";
import Register from './component/login/Register';
import Home from "./component/login/Home";
import Home2 from "./component/login/Home2"


const App = () => {
    
    return (

        <BrowserRouter>
            <div className="App">
                <Home/>
                <div>
                    <Route exact path="/Home2" component={Home2} />
                    <Route path="/Login" component={Login} />
                    <Route path="/Register" component={Register} />
                </div>
            </div>
        </BrowserRouter>
    );
}
{/* 
// <div className="App">
//   {/* <OneClock /> */}
//   {/* <UserProfile />
//   <TryHook />
//   <UseEffect/> */}
//   {/* <ConRendering/> */}
{/* //   <Form/>
// </div>     */}

{/* // <div className="App">
//   <h1>Daftar Hadir Peserta</h1>
//   <button onclick="activateLasers()" color="btn-bnt primary">
//     Aktivasi Laser
//   </button>
//   <Button color="success">success</Button>{' '}

// </div> */}

//   );
// } */}

export default App;