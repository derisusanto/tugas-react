import React from "react";

class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { fname: 'Nama Depan', lname: 'Nama Belakang' };
        this.handleChange1 = this.handleChange1.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }

    handleChange1(event) { 
        this.setState({ fname: event.target.value }); 
    }
    handleChange2(event) {
         this.setState({ lname: event.target.value }); 
    }
    handleSubmit(event) {
        alert('Nama Saya Adalah ' + this.state.fname +' '+ this.state.lname);
        // alert('A name was submitted: ' + this.state.value2);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name Depan:
                    <input type="text" onChange={this.handleChange1} />
                </label>
                <label>
                    Name Belakang:
                    <input type="text"  onChange={this.handleChange2} />
                </label>
                <input type="file" name = "filetag" onchange="changeimage()"></input>
                <div>
                    <input type="submit" value="Submit" />
                </div>
            </form>
        );
    }
}

export default NameForm