import React from "react";

const avatar = props => (
  <img src={props.image} alt="profile"></img>
);

export default avatar