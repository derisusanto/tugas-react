import React from "react";

const bio = props => (
  <h4 className="bg-primary text-white text-center p-2">{props.bio}</h4>
);

export default bio