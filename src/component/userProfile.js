import React from "react";
import Avatar from "./avatar";
import Bio from "./bio";
import UserName from "./userName";

const userProfile = () => (
    <b>
        <Avatar className="foto" image="https://www.shareicon.net/data/512x512/2016/05/24/770117_people_512x512.png" /> 
        <UserName name="Deri" />
        <Bio bio="Ini adalah salah satu pelatihan dalam React Js" />
    </b>
  
);


export default userProfile