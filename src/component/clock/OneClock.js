import React from "react";
import { Button, Alert } from 'reactstrap';

class OneClock extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date(), year: 2018 };
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.updateOneClock(), 1000);
    }
    componentWillUnmount() {

    }
    updateOneClock() {
        this.setState({
            date: new Date()
        });
    }

    ubah = () => {
        if (this.state.year == 2018) {
            this.setState({ year: 2019 })
        } else {
            this.setState({ year: 2018 })
        }
    }

    //   ubah  (){ 
    //      this.state.year === '2018' ?  this.setState({ year : 2019 }) : this.setState.({ year : 2019 });
    //     }      
    // perintah di atas adalah arrow function dari function ubah
    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
                <h1>Tahun</h1>
                <h1>{this.state.year}</h1>
                <Button color="info"
                    type="button"
                    onClick={this.ubah}
                >Change Year</Button>
            </div>
        );
    }
}

export default OneClock