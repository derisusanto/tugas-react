import React, { useState } from "react";
import { Button, Alert } from 'reactstrap';

function UsingHook() {
    const [year, setYear] = useState(2018);

    function ubahTahun() {
        year == 2018 ? setYear(2019) : setYear(2018); //"==" adalah perbandingan"
    }

    return (
        <div>
            <p>You Clicked {year} Time</p>
            {/* onClick={() => ubahTahun(year)} ubahTahun adalah function  */}
            <Button onClick={() => ubahTahun(year)}>Clik Me</Button> 
        </div>
    );
}

    //function UsingHook() {
    //     const [count, setCount] = useState(0);

    //     return (
    //         <div>
    //             <p>You Clicked {count} Time</p>
    //             <Button onClick={() => setCount(count + 1)}>Clik Me</Button>
    //         </div>
    //     );
    //}
    //coba yang ada di modul


    export default UsingHook