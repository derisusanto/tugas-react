import React from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";


const Home =()=>{
    return(
        <div>
            <nav>
                <Link to="/" className="logo">tugasReact</Link>
                <ul className="nav-links"> 
                    <li>
                        <Link to="/Home2" >About</Link>
                    </li>
                    
                    <li>
                        <Link to="/Login">Login</Link>
                    </li>
                    
                    <li>
                        <Link to="/Register" >Registrasi</Link>
                    </li>
                
                </ul>
            </nav>
        </div>
    );
}
export default Home