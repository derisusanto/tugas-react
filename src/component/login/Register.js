import React, { useState } from "react";
import { Button } from "reactstrap";
import Login from "./Login";
import { Link } from "react-router-dom"

const Register = () => {

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const [errorUsername, setErrorUsername] = useState('');
    const [errorEmail, setErrorEmail] = useState('');
    const [errorPassword, setErrorPassword] = useState('');
    const [errorConfirmPassword, setErrorConfirmPassword] = useState('');


    const onChangeusername = (e) => {
        const value = (e.target.value)
        setUsername(value)
        if (!value) {
            setErrorUsername("Username tidak boleh kosong")
        } else {
            setErrorUsername("")
        }
    }

    const onChangeemail = (e) => {
        const value = (e.target.value)
        setEmail(value)
        if (!value) {
            setErrorEmail("Email tidak boleh kosong")
        } else if(!email.includes("@")) {
            setErrorEmail("email tidak valid")
        }else{
            setErrorEmail("") 
        }
    }

    const onChangepassword = (e) => {
        const value = (e.target.value)
        setPassword(value)
        if (!value) {
            setErrorPassword("password tidak boleh kosong")
        } else {
            setErrorPassword("")
        }
    }

    const onChangeconfirmPassword = (e) => {
        const value = (e.target.value)
        setConfirmPassword(value)
        if (!value) {
            setErrorConfirmPassword("Konfirmasi password tidak boleh kosong")
        } else if (password !== value){
            setErrorConfirmPassword("Password tidak cocok !")
        }else {
            setErrorConfirmPassword("")
        }
    }

    const handleSubmit = (e) => {
        alert('Username :' + username + ' ' + 'Email :' + email + ' ' + 'Password :' + password + ' ' + 'Konfrimasi Password :' + confirmPassword);

    }

    return (
        <div style={{backgroundColor: ""}}>
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-5 ">
                <div className="card p-8 ">
                <div className="card-body">
                    <div className="row justify-content-center ">
                        <div className="form" style={{fontstyle : "initial"}}>
                            <h1>
                                <span className="font-weight-bold">Registrasi</span>
                            </h1>
                        </div>
                    </div>
                        <form style={{backgroundColor: "sky blue"}} onSubmit={handleSubmit}>

                            <div className="form-group">
                                <label> Username </label>
                                <input type="text" placeholder="Username" className="form-control my-input" value={username} onChange={onChangeusername} />
                                {
                                    errorUsername && (
                                        <p className="text-danger"> {errorUsername} </p>
                                    )
                                }
                            </div>

                            <div className="form-group">
                                <label> Email </label>
                                <input type="email" placeholder="Email" className="form-control my-input" value={email} onChange={onChangeemail} />
                                {
                                    errorEmail && (
                                        <p className="text-danger"> {errorEmail} </p>
                                    )
                                }
                            </div>

                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" placeholder="Password" className="form-control my-input" value={password} onChange={onChangepassword} />
                                {
                                    errorPassword && (
                                        <p className="text-danger"> {errorPassword} </p>
                                    )
                                }
                            </div>

                            <div className="form-group">
                                <label> Konfirmasi Password </label>
                                <input type="password" placeholder="Konfirmasi Password" className="form-control my-input" value={confirmPassword} onChange={onChangeconfirmPassword} />
                                {
                                    errorConfirmPassword && (
                                        <p className="text-danger"> {errorConfirmPassword} </p>
                                    )
                                }
                            </div>

                                <Button type="submit" className="btn btn-block send-button tx-tfm">REGISTASI</Button>
                            
                            <div className="text-center pt-3">sudah punya akun?
                                <Link to="/Login">Login</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    );
}


export default Register