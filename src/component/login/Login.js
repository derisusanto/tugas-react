import React, { useState } from "react";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";

const Login = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errorUsername, setErrorUsername] = useState('');
    const [errorPassword, setErrorPassword] = useState('');

    const onChangeusername = (e) => {
        const value = (e.target.value)
        setUsername(value)
        if (!value) {
            setErrorUsername("username tidak boleh kosong")
        } else {
            setErrorUsername("")
        }
    }
    const onChangepassword = (e) => {
        const value = (e.target.value)
        setPassword(value)
        if (!value) {
            setErrorPassword("password tidak boleh kosong")
        } else{
            setErrorPassword("")
        }
    }
    const handleSubmit = (e) => {
        alert('Username :' + username + ' ' + 'Password :' + password);

    }

    return (

    <div style={{backgroundColor: ""}}>
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-4 ">
                    <div className="card p-7 ">
                        <div className="card-body">
                    <div className="row justify-content-center ">
                        <div className="form-wight-bold" style={{fontstyle : "initial"}}>
                            <h1>LOGIN</h1>
                        </div>
                    </div>
                        <form  onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label> Username </label>
                                    <input  type="text" placeholder="Username" className="form-control" value={username} onChange={onChangeusername} />
                                    {
                                        errorUsername && (
                                            <p className="text-danger"> {errorUsername} </p>
                                        )
                                    }
                                </div>

                                <div className="form-group">
                                    <label>Password</label>
                                    <input type="password" placeholder="Password" className="form-control" value={password} onChange={onChangepassword} />
                                    {
                                        errorPassword && (
                                            <p className="text-danger"> {errorPassword} </p>
                                        )
                                    }
                                </div>

                                <Button type="submit" className="btn btn-block send-button tx-tfm">Login</Button>
                                <div className="text-center pt-3">Belum punya akun?
                                <Link to="/Register">Registasi</Link>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
}




export default Login 