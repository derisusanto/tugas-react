import React from "react";

const userName = props => (
  <h4 className="bg-primary text-white text-center p-2">{props.name}</h4>
);

export default userName